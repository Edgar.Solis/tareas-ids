var ivan = {
	nom: 'ivan',
	app: 'solis',
	ed: 10
}
var edgar = {
	nom: 'edgar',
	app: 'Hernanadez',
	ed: 20
}
function impMay({nom}){
	
	console.log(nom.toUpperCase())

}
impMay({ nom: 'ivan' })


function feliz(person){
	return{
		...ivan, 
		ed: ivan.ed + 1
	}
}

// feliz(ivan.ed)
// console.log(feliz)
const MAYORIA_EDAD = 18
//puedes declara una variable y asignarle una función,
//se llama función anonima y se puede declarar como constante (const)
//var Mayyor = function (persona){ 
// function mmayor(persona){
//	return persona.ed >= MAYORIA_EDAD
//}
// arriba y abajo es exactamente lo mismo
//********* Ahorro de caracteres******//
// cuando solo retornas un valor puedes eliminar parentesis y llaves ejemplo

const Mayyor = ({ed}) => ed >= MAYORIA_EDAD
const menor = ({ed}) => ed < MAYORIA_EDAD

function mayor (persona){
	if (!menor(persona.ed)){
		console.log(`${persona.nom} es mayor de edad y tiene ${persona.ed}`)
	}
	else {
		console.log(`${persona.nom} es menor de edad y tiene ${persona.ed}`)
	}
}

const edad = 35;
const may = ({age}) => edad > mayor;

function mayor (people) 

