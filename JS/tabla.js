var personArr = [
    {
        "personID": 123,
        "name": "John",
        "city": "Melbourne",
        "phoneNo": "1234567890"
    },
    {
        "personID": 124,
        "name": "Amelia",
        "city": "Sydney",
        "phoneNo": "1234567890"
    },
    {
        "personID": 125,
        "name": "Emily",
        "city": "Perth",
        "phoneNo": "1234567890"
    },
    {
        "personID": 126,
        "name": "Abraham",
        "city": "Perth",
        "phoneNo": "1234567890"
    }
];

var tbody = document.getElementById('tbody');
var tr = "<tr>";
for(let b = 0; b < personArr.length; b++) {
    var tr = "<tr>";
    tr += "<td>" + personArr[b].personID.toString() + "</td>" + "<td>" + personArr[b].name + " </td>" + "<td>" + personArr[b].city + "</td></tr>";
    tbody.innerHTML += tr;
}