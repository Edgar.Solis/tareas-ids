// let promise1 = new Promise((resolve, reject) => {
//     setTimeout(function (){
//         resolve("resolucion correcta!!");
//     }, 250);
// });

// promise1.then( mensaje => {
//     console.log(`Mensaje de resolución; ${mensaje}`);
// });


// let promise2 = new Promise(function(resolve,reject){
//     setTimeout(function(){
//         reject('Done!');
//     }, 250);
// })
// .then( function(e){ console.log('done', e); })
// .catch( function(e){console.log('catch', e); });
//  console.log(promise2);


// function divide(dividiendo, divisor){
//     return new Promise((resolve, reject) => {
//         if(divisor === 0){
//             reject(new Error('No se puede dividir entre 0'));
//         }else{
//             resolve(dividiendo/divisor);
//         }
//     });
// }

// try{
//     const result = divide(5,1);
//     console.log(result);
// } catch(err){
//     console.log(err.message);
// }


function suma(a,b){
    return new Promise(function(resolve, reject){
        resolve(a + b);
    })
}

function restar(valor,cantidad){
    return new Promise(function(resolve, reject){
        resolve(valor - cantidad);
    })
}
function esMayorACero(valor){
    return new Promise(function(resolve,reject){
        if(valor>0){
            resolve('mayor que 0');
        }
        else{
            resolve('menor que 0');
        }
    })
}

const x = 10;
const y = 20;

suma(x,y)
.then(function(resultado_suma){
    return restar(resultado_suma, 15)
})
.then(function(resultado_de_resta){
    return esMayorACero(resultado_de_resta)
})
.then(function (respuesta){
    console.log(respuesta)
})