function persona(nombre, apellido, altura){
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
}
persona.prototype.imprimir = function() {
    if(this.altura < 1.8){
    console.log(`Hola soy ${this.nombre} ${this.apellido} y soy de baja estatura`)
    }
    else{
        console.log(`Hola soy ${this.nombre} ${this.apellido} y soy de alto`)
    }
}
var ivan = new persona('ivan', 'Solis', 1.99)
var gael = new persona('gael', 'Solis', 1.70)
var edgar = new persona('edgar', 'Solis', 1.69)


